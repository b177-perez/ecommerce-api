const router = require("express").Router();
// const stripe = require("stripe")(process.env.STRIPE_KEY);
//const KEY = process.env.STRIPE_KEY

const KEY = "sk_test_51LLOX4Ft46hbrD6HQUKR1FkZPaft88bPMnbyIHyVQ0ci6FQmY4KtlVJij1bBsRb6c8vcHEkoQpZRxXtb6d5iiLG1009GGq9MWY";

const stripe = require("stripe")(KEY);

router.post("/payment", (req, res) => {
  stripe.charges.create(
    {
      source: req.body.tokenId,
      amount: req.body.amount,
      currency: "php",
    },
    (stripeErr, stripeRes) => {
      if (stripeErr) {
        res.status(500).json(stripeErr);
      } else {
        res.status(200).json(stripeRes);
      }
    }
  );
});

module.exports = router;