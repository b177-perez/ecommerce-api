const Cart = require("../models/Cart");

const jwt = require("jsonwebtoken");

const bcrypt = require("bcrypt");

const secret = "EcommerceAPI";

const {
  verifyToken,
  verifyTokenAndAuthorization,
  verifyTokenAndAdmin,
} = require("./verifyToken");

const router = require("express").Router();



// Token Verification
/*
- Analogy
    Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/
function verify (req, res, next){

    // The token is retrieved from the request header
    // This can be provided in postman under
        // Authorization > Bearer Token
    let token = req.headers.authorization;

    // Token recieved and is not undefined
    if (typeof token !== "undefined") {

        console.log(token);

        // The "slice" method takes only the token from the information sent via the request header
        // The token sent is a type of "Bearer" token which when recieved contains the word "Bearer " as a prefix to the string
        // This removes the "Bearer " prefix and obtains only the token for verification
        token = token.slice(7, token.length);

        // Validate the token using the "verify" method decrypting the token using the secret code
        return jwt.verify(token, secret, (err, data) => {

            // If JWT is not valid
            if (err) {

                return res.send({auth : "failed"});

            // If JWT is valid
            } else {

                // Allows the application to proceed with the next middleware function/callback function in the route
                // The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
                next();

            }
        })

    // Token does not exist
    } else {

        return res.send({auth : "failed"});

    };

};



// Token decryption
/*
- Analogy
    Open the gift and get the content
*/
function decode(token){

    // Token recieved and is not undefined
    if(typeof token !== "undefined"){

        // Retrieves only the token and removes the "Bearer " prefix
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if (err) {

                return null;

            } else {

                // The "decode" method is used to obtain the information from the JWT
                // The "{complete:true}" option allows us to return additional information from the JWT token
                // Returns an object with access to the "payload" property which contains user information stored when the token was generated
                // The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
                return jwt.decode(token, {complete:true}).payload;
            };

        })

    // Token does not exist
    } else {

        return null;

    };

};







//CREATE

// router.post("/", verifyToken, async (req, res) => {
//   const newCart = new Cart(req.body);

//   try {
//     const savedCart = await newCart.save();
//     res.status(200).json(savedCart);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });



// NEW CREATE

// Route for creating a product
router.post("/", verify, (req, res) => {

  const data = {
    cart: req.body,
    isAdmin: decode(req.headers.authorization).isAdmin
  }


  function addCart(data){

  // User is an admin
  if (data.isAdmin === false) {

    // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    // Uses the information from the request body to provide all the necessary information
    

    const newCart = new Cart(req.body);

    // Saves the created object to our database
    return newCart.save().then((cart, error) => {

      // Course creation successful
      if (error) {

        return false;

      // Course creation failed
      } else {

        return newCart;

      };

    });

  // User is not an admin
  } else {
    return Promise.resolve(false);
  };
  

};

  addCart(data).then(resultFromController => res.send(resultFromController));

});



//UPDATE
// router.put("/:id", verifyTokenAndAuthorization, async (req, res) => {
//   try {
//     const updatedCart = await Cart.findByIdAndUpdate(
//       req.params.id,
//       {
//         $set: req.body,
//       },
//       { new: true }
//     );
//     res.status(200).json(updatedCart);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });


// NEW UPDATE

router.put("/:id", verify, (req, res) => {

  const data = {
    cart: req.body,
    isAdmin: decode(req.headers.authorization).isAdmin
  }

function updateCart(reqParams, reqBody){

if (data.isAdmin === false) {

  // Specify the fields/properties of the document to be updated
  let updatedCart = {
    $set: req.body
  };

  // Syntax
    // findByIdAndUpdate(document ID, updatesToBeApplied)
  return Cart.findByIdAndUpdate(reqParams.id, updatedCart).then((cart, error) => {

    // Course not updated
    if (error) {

      return false;

    // Course updated successfully
    } else {

      return updatedCart;
    };

  });


} else {
    return Promise.resolve(false);
};

};


  updateCart(req.params, req.body).then(resultFromController => res.send(resultFromController));

});







//DELETE
// router.delete("/:id", verifyTokenAndAuthorization, async (req, res) => {
//   try {
//     await Cart.findByIdAndDelete(req.params.id);
//     res.status(200).json("Cart has been deleted...");
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });


// NEW DELETE

router.delete("/:id/delete", verify, (req, res) => {

const data = {
    cart: req.body,
    isAdmin: decode(req.headers.authorization).isAdmin
  }


  function deleteCart(reqParams){

if (data.isAdmin === false) {    


  return Cart.findByIdAndDelete(reqParams.id).then((cart, error) => {

    // Course not archived
    if (error) {

      return false;

    // Course archived successfully
    } else {

      return true;

    }

  });

} else {
    return Promise.resolve(false);
};


};


  deleteCart(req.params, req.body).then(resultFromController => res.send(resultFromController));
  
});





//GET USER CART
// router.get("/find/:userId", verifyTokenAndAuthorization, async (req, res) => {
//   try {
//     const cart = await Cart.findOne({ userId: req.params.userId });
//     res.status(200).json(cart);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });


//NEW GET USER CART


router.get("/find/:userId", verify, (req, res) => {

  const data = {
    userId : decode(req.headers.authorization).id,
    isAdmin: decode(req.headers.authorization).isAdmin
  }

  console.log(data);


  function getMyCart(data){

  if(data.isAdmin === false) {

    return Cart.find({ userId : data.userId}).then(result => {
      return result;
    })

  }

  
  else{
    
    return Promise.resolve(false);

  }
  
}

  getMyCart(data).then(resultFromController => res.send(resultFromController));
});



//GET ALL CART

// router.get("/", verifyTokenAndAdmin, async (req, res) => {
//   try {
//     const carts = await Cart.find();
//     res.status(200).json(carts);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });


//NEW GET ALL CART

router.get("/all", verify, (req, res) => {

  const data = {
   // userId : auth.decode(req.headers.authorization).id,
    isAdmin : decode(req.headers.authorization).isAdmin
  }

  console.log(data);


  function getAllCarts(data){


  if(data.isAdmin) {

    return Cart.find({}).then(result => {
      return result;
    })

  }

  // Non-Admin Cannot Retrieve ALL Orders
  else{
    return Promise.resolve(false + " - Only Admin users can retrieve all carts.");
  }
  
}



  getAllCarts(data).then(resultFromController => res.send(resultFromController));
});




module.exports = router;