const router = require("express").Router();
const User = require("../models/User");
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const secret = "EcommerceAPI";

function createAccessToken(user){
    // The data will be received from the registration form
    // When the user logs in, a token will be created with user's information
    const data = {
        id : user._id,
        username : user.username,
        isAdmin : user.isAdmin
    };

    // Generate a JSON web token using the jwt's sign method
    // Generates the token using the form data and the secret code with no additional options provided
    return jwt.sign(data, secret, {});
    
};


//REGISTER
// router.post("/register", async (req, res) => {
//   const newUser = new User({
//     username: req.body.username,
//     email: req.body.email,
//     password: CryptoJS.AES.encrypt(
//       req.body.password,
//       process.env.PASS_SEC
//     ).toString(),
//   });

//   try {
//     const savedUser = await newUser.save();
//     res.status(201).json(savedUser);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });



// NEW REGISTER

// Route for user registration
router.post("/register", (req, res) => {

    function registerUser(reqBody){

    // Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
    // Uses the information from the request body to provide all the necessary information
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        username : reqBody.username,
        email : reqBody.email,
        // 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
        password : bcrypt.hashSync(reqBody.password, 10)
    })

    // Saves the created object to our database
    return newUser.save().then((user, error) => {

        // User registration failed
        if (error) {

            return false;

        // User registration successful

        } else {

            return true;

        };

    });

};

    registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


//LOGIN

// router.post('/login', async (req, res) => {
//     try{
//         const user = await User.findOne(
//             {
//                 userName: req.body.user_name
//             }
//         );

//         !user && res.status(401).json("Wrong User Name");

//         const hashedPassword = CryptoJS.AES.decrypt(
//             user.password,
//             process.env.PASS_SEC
//         );


//         const originalPassword = hashedPassword.toString(CryptoJS.enc.Utf8);

//         const inputPassword = req.body.password;
        
//         originalPassword != inputPassword && 
//             res.status(401).json("Wrong Password");

//         const accessToken = jwt.sign(
//         {
//             id: user._id,
//             isAdmin: user.isAdmin,
//         },
//         process.env.JWT_SEC,
//             {expiresIn:"3d"}
//         );
  
//         const { password, ...others } = user._doc;  
//         res.status(200).json({...others, accessToken});

//     }catch(err){
//         res.status(500).json(err);
//     }

// });


// NEW LOGIN

// Route for user authentication
router.post("/login", (req, res) => {

    function loginUser(reqBody){
    // The "findOne" method returns the first record in the collection that matches the search criteria
    // We use the "findOne" method instead of the "find" method which returns all records that match the search criteria
    return User.findOne({username : reqBody.username}).then(result => {

        // User does not exist
        if(result == null){

            return false;

        // User exists
        } else {

            console.log('reqBody.password: ' + reqBody.password);
            console.log('result.password: ' + result.password);

            // Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
            // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
            // A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
                //example. isSingle, isDone, isAdmin, areDone, etc..
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            

            console.log('isPasswordCorrect: ' + isPasswordCorrect);

            // If the passwords match/result of the above code is true
            if (isPasswordCorrect) {

                // Generate an access token
                // Uses the "createAccessToken" method defined in the "auth.js" file
                // Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
                return { access : createAccessToken(result) }

            // Passwords do not match
            } else {

                return false;

            };

        };

    });

};


    loginUser(req.body).then(resultFromController => res.send(resultFromController));
});




// Token Verification
/*
- Analogy
    Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/
function verify (req, res, next){

    // The token is retrieved from the request header
    // This can be provided in postman under
        // Authorization > Bearer Token
    let token = req.headers.authorization;

    // Token recieved and is not undefined
    if (typeof token !== "undefined") {

        console.log(token);

        // The "slice" method takes only the token from the information sent via the request header
        // The token sent is a type of "Bearer" token which when recieved contains the word "Bearer " as a prefix to the string
        // This removes the "Bearer " prefix and obtains only the token for verification
        token = token.slice(7, token.length);

        // Validate the token using the "verify" method decrypting the token using the secret code
        return jwt.verify(token, secret, (err, data) => {

            // If JWT is not valid
            if (err) {

                return res.send({auth : "failed"});

            // If JWT is valid
            } else {

                // Allows the application to proceed with the next middleware function/callback function in the route
                // The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
                next();

            }
        })

    // Token does not exist
    } else {

        return res.send({auth : "failed"});

    };

};



// Token decryption
/*
- Analogy
    Open the gift and get the content
*/
function decode(token){

    // Token recieved and is not undefined
    if(typeof token !== "undefined"){

        // Retrieves only the token and removes the "Bearer " prefix
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if (err) {

                return null;

            } else {

                // The "decode" method is used to obtain the information from the JWT
                // The "{complete:true}" option allows us to return additional information from the JWT token
                // Returns an object with access to the "payload" property which contains user information stored when the token was generated
                // The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
                return jwt.decode(token, {complete:true}).payload;
            };

        })

    // Token does not exist
    } else {

        return null;

    };

};




router.get("/details", verify, (req, res) => {

    // Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
    const userData = decode(req.headers.authorization);



    // Retrieve user details
/*
    Steps:
    1. Find the document in the database using the user's ID
    2. Reassign the password of the returned document to an empty string
    3. Return the result back to the frontend
*/
function getProfile(data){

    return User.findById(data.userId).then(result => {

        // Changes the value of the user's password to an empty string when returned to the frontend
        // Not doing so will expose the user's password which will also not be needed in other parts of our application
        // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
        result.password = "";

        // Returns the user information with the password as an empty string
        return result;

    });

};




    // Provides the user's ID for the getProfile controller method
    getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});





module.exports = router;