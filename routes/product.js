const Product = require("../models/Product");

const jwt = require("jsonwebtoken");

const bcrypt = require("bcrypt");

const secret = "EcommerceAPI";

const {
  verifyToken,
  verifyTokenAndAdmin
} = require("./verifyToken");

//const { verify } = require("./auth");

const router = require("express").Router();

//CREATE

// router.post("/", verifyTokenAndAdmin, async (req, res) => {
//   const newProduct = new Product(req.body);

//   try {
//     const savedProduct = await newProduct.save();
//     res.status(200).json(savedProduct);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });



// Token Verification
/*
- Analogy
    Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/
function verify (req, res, next){

    // The token is retrieved from the request header
    // This can be provided in postman under
        // Authorization > Bearer Token
    let token = req.headers.authorization;

    // Token recieved and is not undefined
    if (typeof token !== "undefined") {

        console.log(token);

        // The "slice" method takes only the token from the information sent via the request header
        // The token sent is a type of "Bearer" token which when recieved contains the word "Bearer " as a prefix to the string
        // This removes the "Bearer " prefix and obtains only the token for verification
        token = token.slice(7, token.length);

        // Validate the token using the "verify" method decrypting the token using the secret code
        return jwt.verify(token, secret, (err, data) => {

            // If JWT is not valid
            if (err) {

                return res.send({auth : "failed"});

            // If JWT is valid
            } else {

                // Allows the application to proceed with the next middleware function/callback function in the route
                // The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
                next();

            }
        })

    // Token does not exist
    } else {

        return res.send({auth : "failed"});

    };

};



// Token decryption
/*
- Analogy
    Open the gift and get the content
*/
function decode(token){

    // Token recieved and is not undefined
    if(typeof token !== "undefined"){

        // Retrieves only the token and removes the "Bearer " prefix
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if (err) {

                return null;

            } else {

                // The "decode" method is used to obtain the information from the JWT
                // The "{complete:true}" option allows us to return additional information from the JWT token
                // Returns an object with access to the "payload" property which contains user information stored when the token was generated
                // The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
                return jwt.decode(token, {complete:true}).payload;
            };

        })

    // Token does not exist
    } else {

        return null;

    };

};



// NEW CREATE

// Route for creating a product
router.post("/", verify, (req, res) => {

  const data = {
    product: req.body,
    isAdmin: decode(req.headers.authorization).isAdmin
  }


  function addProduct(data){

  // User is an admin
  if (data.isAdmin) {

    // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    // Uses the information from the request body to provide all the necessary information
    // let newProduct = new Product({
    //   title : data.product.title,
    //   desc : data.product.desc,
    //   img : data.product.img,
    //   categories : data.product.categories,
    //   size : data.product.size,
    //   color : data.product.color,
    //   price : data.product.price
    // });

    const newProduct = new Product(req.body);

    // Saves the created object to our database
    return newProduct.save().then((product, error) => {

      // Course creation successful
      if (error) {

        return false;

      // Course creation failed
      } else {

        return newProduct;

      };

    });

  // User is not an admin
  } else {
    return Promise.resolve(false);
  };
  

};

  addProduct(data).then(resultFromController => res.send(resultFromController));

});



//UPDATE
// router.put("/:id", verifyTokenAndAdmin, async (req, res) => {
//   try {
//     const updatedProduct = await Product.findByIdAndUpdate(
//       req.params.id,
//       {
//         $set: req.body,
//       },
//       { new: true }
//     );
//     res.status(200).json(updatedProduct);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });


// NEW UPDATE

router.put("/:id", verify, (req, res) => {

  const data = {
    product: req.body,
    isAdmin: decode(req.headers.authorization).isAdmin
  }

function updateProduct(reqParams, reqBody){

if (data.isAdmin) {

  // Specify the fields/properties of the document to be updated
  let updatedProduct = {
    $set: req.body
  };

  // Syntax
    // findByIdAndUpdate(document ID, updatesToBeApplied)
  return Product.findByIdAndUpdate(reqParams.id, updatedProduct).then((product, error) => {

    // Course not updated
    if (error) {

      return false;

    // Course updated successfully
    } else {

      return updatedProduct;
    };

  });


} else {
    return Promise.resolve(false);
};

};


  updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));

});





//DELETE
// router.delete("/:id", verifyTokenAndAdmin, async (req, res) => {
//   try {
//     await Product.findByIdAndDelete(req.params.id);
//     res.status(200).json("Product has been deleted...");
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });



// NEW DELETE - ARCHIVE Deactivate/Reactivate

router.put("/:id/archive", verify, (req, res) => {

const data = {
    product: req.body,
    isAdmin: decode(req.headers.authorization).isAdmin
  }


  function archiveProduct(reqParams){

if (data.isAdmin) {    

  let updateInStockField = {
    inStock : false
  };

  return Product.findByIdAndUpdate(reqParams.id, updateInStockField).then((product, error) => {

    // Course not archived
    if (error) {

      return false;

    // Course archived successfully
    } else {

      return true;

    }

  });

} else {
    return Promise.resolve(false);
};


};


  archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
  
});





//GET PRODUCT
router.get("/find/:id", async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);
    res.status(200).json(product);
  } catch (err) {
    res.status(500).json(err);
  }
});

//GET ALL PRODUCTS
router.get("/", async (req, res) => {
  const qNew = req.query.new;
  const qCategory = req.query.category;
  try {
    let products;

    if (qNew) {
      products = await Product.find().sort({ createdAt: -1 }).limit(1);
    } else if (qCategory) {
      products = await Product.find({
        categories: {
          $in: [qCategory],
        },
      });
    } else {
      products = await Product.find();
    }

    res.status(200).json(products);
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router;
