const Order = require("../models/Order");
const {
  verifyToken,
  verifyTokenAndAuthorization,
  verifyTokenAndAdmin,
} = require("./verifyToken");

const jwt = require("jsonwebtoken");

const bcrypt = require("bcrypt");

const secret = "EcommerceAPI";

const router = require("express").Router();




// Token Verification
/*
- Analogy
    Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/
function verify (req, res, next){

    // The token is retrieved from the request header
    // This can be provided in postman under
        // Authorization > Bearer Token
    let token = req.headers.authorization;

    // Token recieved and is not undefined
    if (typeof token !== "undefined") {

        console.log(token);

        // The "slice" method takes only the token from the information sent via the request header
        // The token sent is a type of "Bearer" token which when recieved contains the word "Bearer " as a prefix to the string
        // This removes the "Bearer " prefix and obtains only the token for verification
        token = token.slice(7, token.length);

        // Validate the token using the "verify" method decrypting the token using the secret code
        return jwt.verify(token, secret, (err, data) => {

            // If JWT is not valid
            if (err) {

                return res.send({auth : "failed"});

            // If JWT is valid
            } else {

                // Allows the application to proceed with the next middleware function/callback function in the route
                // The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
                next();

            }
        })

    // Token does not exist
    } else {

        return res.send({auth : "failed"});

    };

};



// Token decryption
/*
- Analogy
    Open the gift and get the content
*/
function decode(token){

    // Token recieved and is not undefined
    if(typeof token !== "undefined"){

        // Retrieves only the token and removes the "Bearer " prefix
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if (err) {

                return null;

            } else {

                // The "decode" method is used to obtain the information from the JWT
                // The "{complete:true}" option allows us to return additional information from the JWT token
                // Returns an object with access to the "payload" property which contains user information stored when the token was generated
                // The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
                return jwt.decode(token, {complete:true}).payload;
            };

        })

    // Token does not exist
    } else {

        return null;

    };

};




//CREATE

// router.post("/", verifyToken, async (req, res) => {
//   const newOrder = new Order(req.body);

//   try {
//     const savedOrder = await newOrder.save();
//     res.status(200).json(savedOrder);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });


// NEW CREATE

// Route for creating a product
router.post("/", verify, (req, res) => {

  const data = {
    order: req.body,
    isAdmin: decode(req.headers.authorization).isAdmin
  }


  function addOrder(data){

  // User is an admin
  if (data.isAdmin === false) {

    // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    // Uses the information from the request body to provide all the necessary information
    

    const newOrder = new Order(req.body);

    // Saves the created object to our database
    return newOrder.save().then((order, error) => {

      // Course creation successful
      if (error) {

        return false;

      // Course creation failed
      } else {

        return newOrder;

      };

    });

  // User is an admin
  } else {
    return Promise.resolve(false);
  };
  

};

  addOrder(data).then(resultFromController => res.send(resultFromController));

});



//UPDATE
// router.put("/:id", verifyTokenAndAdmin, async (req, res) => {
//   try {
//     const updatedOrder = await Order.findByIdAndUpdate(
//       req.params.id,
//       {
//         $set: req.body,
//       },
//       { new: true }
//     );
//     res.status(200).json(updatedOrder);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });



// NEW UPDATE

router.put("/:id", verify, (req, res) => {

  const data = {
    order: req.body,
    isAdmin: decode(req.headers.authorization).isAdmin
  }

function updateOrder(reqParams, reqBody){

if (data.isAdmin) {

  // Specify the fields/properties of the document to be updated
  let updatedOrder = {
    $set: req.body
  };

  // Syntax
    // findByIdAndUpdate(document ID, updatesToBeApplied)
  return Order.findByIdAndUpdate(reqParams.id, updatedOrder).then((order, error) => {

    // Course not updated
    if (error) {

      return false;

    // Course updated successfully
    } else {

      return updatedOrder;
    };

  });


} else {
    return Promise.resolve(false);
};

};


  updateOrder(req.params, req.body).then(resultFromController => res.send(resultFromController));

});




//DELETE
// router.delete("/:id", verifyTokenAndAdmin, async (req, res) => {
//   try {
//     await Order.findByIdAndDelete(req.params.id);
//     res.status(200).json("Order has been deleted...");
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });


// NEW DELETE

router.delete("/:id/delete", verify, (req, res) => {

const data = {
    order: req.body,
    isAdmin: decode(req.headers.authorization).isAdmin
  }


  function deleteOrder(reqParams){

if (data.isAdmin) {    


  return Order.findByIdAndDelete(reqParams.id).then((order, error) => {

    // Course not archived
    if (error) {

      return false;

    // Course archived successfully
    } else {

      return true;

    }

  });

} else {
    return Promise.resolve(false);
};


};


  deleteOrder(req.params, req.body).then(resultFromController => res.send(resultFromController));
  
});





//GET USER ORDERS
// router.get("/find/:userId", verifyTokenAndAuthorization, async (req, res) => {
//   try {
//     const orders = await Order.find({ userId: req.params.userId });
//     res.status(200).json(orders);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });


//NEW GET USER ORDER


router.get("/find/:userId", verify, (req, res) => {

  const data = {
    userId : decode(req.headers.authorization).id,
    isAdmin: decode(req.headers.authorization).isAdmin
  }

  console.log(data);


  function getMyOrder(data){

  if(data.isAdmin === false) {

    return Order.find({ userId : data.userId}).then(result => {
      return result;
    })

  }

  
  else{
    
    return Promise.resolve(false);

  }
  
}

  getMyOrder(data).then(resultFromController => res.send(resultFromController));
});



//GET ALL ORDERS

// router.get("/", verifyTokenAndAdmin, async (req, res) => {
//   try {
//     const orders = await Order.find();
//     res.status(200).json(orders);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });



//NEW GET ALL ORDERS

router.get("/all", verify, (req, res) => {

  const data = {
   // userId : auth.decode(req.headers.authorization).id,
    isAdmin : decode(req.headers.authorization).isAdmin
  }

  console.log(data);


  function getAllOrders(data){


  if(data.isAdmin) {

    return Order.find({}).then(result => {
      return result;
    })

  }

  // Non-Admin Cannot Retrieve ALL Orders
  else{
    return Promise.resolve(false + " - Only Admin users can retrieve all carts.");
  }
  
}



  getAllOrders(data).then(resultFromController => res.send(resultFromController));
});




// GET MONTHLY INCOME

router.get("/income", verifyTokenAndAdmin, async (req, res) => {
  const productId = req.query.pid;
  const date = new Date();
  const lastMonth = new Date(date.setMonth(date.getMonth() - 1));
  const previousMonth = new Date(new Date().setMonth(lastMonth.getMonth() - 1));

  try {
    const income = await Order.aggregate([
      {
        $match: {
          createdAt: { $gte: previousMonth },
          ...(productId && {
            products: { $elemMatch: { productId } },
          }),
        },
      },
      {
        $project: {
          month: { $month: "$createdAt" },
          sales: "$amount",
        },
      },
      {
        $group: {
          _id: "$month",
          total: { $sum: "$sales" },
        },
      },
    ]);
    res.status(200).json(income);
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router;